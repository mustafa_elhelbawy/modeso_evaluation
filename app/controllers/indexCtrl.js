'use strict';
app.controller('IndexCtrl', ['$scope', '$mdDialog', function ($scope, $mdDialog) {
  $scope.today = new Date();
  $scope.articles = [];
  for (var i = 0; i < 15; i++) {
    $scope.articles.push({
      // face: imagePath,
      name: "ABC "+ i,
      article: 159+i,
      x: 6+i,
      y: 24+i,
      date: $scope.today.getMonth()+'/'+$scope.today.getDate()+'/'+$scope.today.getFullYear()
    });
  }


  $scope.search = function (article) {
      return (angular.lowercase(article.name).indexOf(angular.lowercase($scope.query) || '') !== -1 ||
              angular.lowercase(article.article).indexOf(angular.lowercase($scope.query) || '') !== -1 ||
              angular.lowercase(article.x).indexOf(angular.lowercase($scope.query) || '') !== -1 ||
              angular.lowercase(article.y).indexOf(angular.lowercase($scope.query) || '') !== -1 ||
              angular.lowercase(article.date).indexOf(angular.lowercase($scope.query) || '') !== -1);
  };


  $scope.showAdd = function(ev) {
    $mdDialog.show({
      controller: DialogController,
      template: '<md-dialog aria-label="Mango (Fruit)"> <md-content class="md-padding"> <form name="userForm"> <div layout layout-sm="column"> <md-input-container flex> <label>Name</label> <input ng-model="user.firstName" placeholder="Name"> </md-input-container> <md-input-container flex> <label>Article</label> <input ng-model="theMax"> </md-input-container> </div> <div layout layout-sm="column"> <md-input-container flex> <label>X</label> <input ng-model="user.city"> </md-input-container> <md-input-container flex> <label>Y</label> <input ng-model="user.state"> </md-input-container> </div> <md-datepicker ng-model="article.date" md-placeholder="Enter date"></md-datepicker> <md-input-container> <label>Input</label> <textarea ng-model="user.biography" columns="1" md-maxlength="150"></textarea> </md-input-container> </form> </md-content> <md-dialog-actions layout="row"> <span flex></span> <md-button ng-click="answer(\'not useful\')"> Cancel </md-button> <md-button ng-click="answer(\'useful\')" class="md-primary"> Save </md-button> </md-dialog-actions></md-dialog>',
      targetEvent: ev,
    })
    .then(function(answer) {
      $scope.alert =  answer ;
    }, function() {
      $scope.alert = 'You cancelled the dialog.';
    });
  };
}]);



function DialogController($scope, $mdDialog) {
  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.answer = function(answer) {
    $mdDialog.hide(answer);
  };
};
